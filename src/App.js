import React from 'react';
import './App.css';
import Console from './Console'

function App() {

  // check for expected pathname, render only if found
  const pathname = window.location.pathname;
  const match = pathname.match(/pipeline\/(.*)/);

  if(match) {
    return (
      <div className="App">
        <Console id={match[1]}/>
      </div>
    );
  } else {
    return <div>No pipeline specified.</div>
  }
}

export default App;
