import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Ansi from "ansi-to-react";
import { api, url } from './config';


function Console({id}) {
    const [data, saveData] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                `${api.console.href}/${id}`
            );

            saveData(result.data);
       }

        fetchData();
    });

    if (!data) {
        return <div>Loading ...</div>
    }

    const splitted_data = data.split('\n');
    const lines = [];

    for (const line in splitted_data) {
        lines.push(
            <Ansi>{splitted_data[line]}</Ansi>
        )
    }

    return (
        <div>
            <h3>Artifacts: <a href={url.artifacts + '/' + id + '/'}>{url.artifacts}/{id}/</a></h3>
            <h3>Console:</h3>
            <div>{lines}</div>
        </div>
    );
}

export default Console;
