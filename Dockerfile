FROM bucharestgold/centos7-s2i-web-app:0.0.2

USER root

ARG API_URL
ARG ARTIFACTS_URL

# required variables
RUN test -n "$API_URL"
RUN test -n "$ARTIFACTS_URL"

WORKDIR /opt/console

ADD . /opt/console

RUN mv /opt/console/src/config.js.example /opt/console/src/config.js

RUN sed -i "s|https://api.url|$API_URL|" /opt/console/src/config.js
RUN sed -i "s|https://artifacts.url|$ARTIFACTS_URL|" /opt/console/src/config.js

RUN yarn cache clean && yarn install && yarn build

RUN chmod -Rf 777 /opt/console

EXPOSE 3000
CMD ["yarn", "start"]
